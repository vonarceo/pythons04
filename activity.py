class Animal():
    def __init__(self):
        self._food = food

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):

        pass
        
        


class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__
        self._name = name
        self._breed = breed
        self._age = age

    # getter

    def get_details(self):
        print(f"I am a Cat, my name is {self._name}, my breed is {self._breed}, and my age is {self._age}")

    # setter
    
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    # call() function

    def call(self):
        print(f"Come Here! {self._name}")         

    def make_sound(self):
        print(f"Meow Meow!!!")               


class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__
        self._name = name
        self._breed = breed
        self._age = age

    # getter

    def get_details(self):
        print(f"I am a Dog, my name is {self._name}, my breed is {self._breed}, and my age is {self._age}")

    # setter
    
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

    # call() function

    def call(self):
        print(f"Come Here! {self._name}")         

    def make_sound(self):
        print(f"Arf Arf!!!")



dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 5)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()